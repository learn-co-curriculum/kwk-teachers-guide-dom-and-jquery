## Objectives

1. Students will gain an understanding of what the Document Object Model
2. Students should be able to manipulate the DOM to create, remove or modify elements and webpage content
3. Students will get exposure to jQuery and how it can be used as a shorter alternative to vanilla JS DOM manipulation


## Resources

### DOM

* [Intro to DOM](https://www.w3schools.com/js/js_htmldom.asp)
* [MDN's DOM Guides](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model)
* [DOM Manipulation, Events, and Animation](https://www.sitepoint.com/dom-manipulation-vanilla-javascript-no-jquery/)
* [HTML, JS and DOM Reference](https://www.w3schools.com/jsref/default.asp)

### jQuery

* [Learn jQuery](https://learn.jquery.com/about-jquery/how-jquery-works/)
* [Intro to jQuery](https://github.com/learn-co-curriculum/js-jquery-modify-html-readme)
* [DOM manipulation in jQuery Reference](https://api.jquery.com/category/manipulation/)
* [Comparison between JS and jQuery DOM methods](https://blog.garstasio.com/you-dont-need-jquery/dom-manipulation/)

